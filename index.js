const express=require('express')
const cors=require('cors')
const mysql=require('mysql2')

const app=express()
app.use(cors("*"))
app.use(express.json())

var conn=mysql.createConnection({
    host:'localhost',
    user:'root',
    password:'manager',
    port:"3306",
    database:'advjava2'
})

app.get('/',(req,res)=>{
    conn.query("select * from students",(err,result)=>{
        if(err!=null){
            res.send(err)
        }
        else{
            res.send(result)
        }
    })
})

app.post('/',(req,res)=>{
    const{id,name,password,course,year,prn,dob}=req.body
    var querry="insert into students(id,name,password,course,year,prn,dob) values(?,?,?,?,?,?,?)"
    conn.execute(querry,[id,name,password,course,year,prn,dob],(err,result)=>{
        if(err!=null)
        res.send(err)
        else
        res.send(result)
    })
})

    app.put('/:no',(req,res)=>{
        var querry =`update students set course='${req.body.course}',prn='${req.body.prn}' where id=${req.params.no}`
        conn.query(querry,(err,result)=>{
            if(err!=null)
            res.send(err)
            else
            res.send(result)
        })
    })

    app.delete('/',(req,res)=>{

        let query = `delete from students where dob = '${req.body.dob}'`;
        conn.query(query, (error, result)=>
        {
          if(error!=null)
           {
            res.send(error);    
            //connection.end();
           }
           else
           {
            res.send(result);
            //connection.end();
           }
    })

})

app.listen(5555,()=>{
    console.log("server started on port 5555")
})